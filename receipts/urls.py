from django.urls import path
from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    CategoriesListView,
    AccountListView,
    CategoryCreateView,
    AccountCreateView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path(
        "categories/",
        CategoriesListView.as_view(),
        name="category_list",
    ),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "categories/create/",
        CategoryCreateView.as_view(),
        name="category_new",
    ),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
]
