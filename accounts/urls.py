from django.urls import path
from django.contrib.auth import views as auth_views
from accounts.views import CreateUserView
from receipts.views import AccountCreateView


urlpatterns = [
    path("login/", auth_views.LoginView.as_view(), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("signup/", CreateUserView.as_view(), name="signup"),
    path("create/", AccountCreateView.as_view(), name="create_account"),
]
